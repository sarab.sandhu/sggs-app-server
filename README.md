# sggs-app-server
sggs application server

## Installation

npm install

## Run sggs app server

npm start

## Run sggs app server in development mode

npm run dev

## test sggs app server

after running the app, use below urls to test app

http://localhost:8000/api/sggs/search?language=english&type=firstInitialLetter&keyword=gmk&skip=0&rowCount=50

http://localhost:8000/api/sggs/search?language=gurmukhi&type=firstInitialLetter&keyword=gmk&skip=0&rowCount=50


